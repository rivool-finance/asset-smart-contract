1. [Introdução](#introdução)
2. [Contratos](#contratos)
   2.1. [SmartTokenV2.sol](#smarttokenv2sol)
   2.2. [ProxyAdmin.sol](#proxyadminsol)
3. [Implantação e interação](#implantação-e-interação)
   3.1. [Scripts de implantação (pasta `scripts`)](#scripts-de-implantação-pasta-scripts)
   3.2. [Interact.js](#interactjs)
4. [Atualizando o contrato](#atualizando-o-contrato)
5. [Testes](#testes)
6. [Como usar](#como-usar)
7. [Conclusão](#conclusão)


# Projeto SmartToken com Proxy

Este projeto implementa um token ERC20 personalizado chamado `SmartToken` com funcionalidades adicionais, como pagamentos com comprovante e saques. O projeto utiliza a biblioteca OpenZeppelin para o padrão ERC20 e os contratos de proxy para facilitar a atualização do contrato.

## Contratos

### SmartTokenV2.sol

`SmartTokenV2` é a versão mais recente do contrato do token. Ele herda de `ERC20` e `Ownable`, ambos provenientes da biblioteca OpenZeppelin. Além das funcionalidades padrão do ERC20, este contrato implementa funções personalizadas, como pagamentos com comprovante e saques.

### ProxyAdmin.sol

`ProxyAdmin` é um contrato fornecido pela OpenZeppelin que permite a administração de proxies no projeto. Ele facilita a atualização e o gerenciamento de contratos proxy.

## Implantação e interação

### Scripts de implantação (pasta `scripts`)

- `deploy.js`: Este script implanta o contrato `SmartTokenV2` e cria um proxy `TransparentUpgradeableProxy` apontando para a implementação do contrato. O endereço do proxy é exibido após a implantação bem-sucedida.

### Interact.js

`interact.js` é um script que permite a interação com o contrato `SmartTokenV2` através do proxy. Ele oferece um menu no terminal para executar as funções do contrato, como mint, approve, transferFrom, pay, withdraw e burnFrom.

## Atualizando o contrato

Para atualizar o contrato `SmartTokenV2`, siga os passos abaixo:

1. Crie uma nova versão do contrato `SmartTokenV2` com as mudanças necessárias e compile o contrato.
2. Atualize o script `upgrade.js` com o novo endereço do contrato atualizado.
3. Execute o script `upgrade.js` usando o comando `npx hardhat run --network localhost scripts/upgrade.js`.

## Testes

Os testes são escritos usando a biblioteca Hardhat e estão localizados na pasta `test`. O arquivo `smarttoken.test.js` contém testes para o contrato `SmartTokenV2`. Para executar os testes, use o comando `npx hardhat test`.

## Como usar

1. Compile os contratos usando o comando `npx hardhat compile`.
2. Implante os contratos e o proxy executando `npx hardhat run --network localhost scripts/deploy.js`.
3. Atualize o arquivo `interact.js` com o endereço do proxy gerado.
4. Inicie uma rede Ethereum local usando o comando `npx hardhat node`.
5. Execute o script `interact.js` para interagir com o contrato usando o comando `node interact.js`.
6. Atualize o contrato `SmartToken.sol`:

`npx hardhat run --network <network_name> scripts/upgrade.js`


## Conclusão

Este projeto demonstra como criar um token personalizado com funcionalidades adicionais e utilizar proxies para permitir atualizações futuras. O uso de contratos de proxy e administradores de proxy ajuda a garantir que os dados do token sejam preservados enquanto as funcionalidades do contrato são atualizadas ou aprimoradas. Para mais informações, consulte a documentação do OpenZeppelin e do Hardhat.
