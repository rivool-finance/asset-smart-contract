// Importa os objetos 'ethers' e 'upgrades' do pacote 'hardhat'
const { ethers, upgrades } = require("hardhat");

// Define a função assíncrona 'main', que será executada para implantar o contrato
async function main() {
  // Obtém a fábrica de contratos do SmartToken, que será usada para criar uma nova instância do contrato
  const Token = await ethers.getContractFactory("SmartToken");
  
  // Exibe no console que o processo de implantação do SmartToken está começando
  console.log("Deploying SmartToken...");

  // Define a variável 'tokenName' como "SmartToken"
  const tokenName = "SmartToken";
  
  // Define a variável 'tokenSymbol' como "BRLTS"
  const tokenSymbol = "BRLTS";
  
  // Define a variável 'tokenImage' como uma URL de imagem
  const tokenImage = "https://imgur.com/a/fuzZCh8";
  
  // Implanta o contrato SmartToken usando um proxy, passando os argumentos 'tokenName', 'tokenSymbol' e 'tokenImage' para a função inicializadora 'initialize'
  const token = await upgrades.deployProxy(Token, [tokenName, tokenSymbol, tokenImage], { initializer: 'initialize' });
  
  // Aguarda até que o contrato seja implantado com sucesso na rede Ethereum
  await token.deployed();

  // Exibe no console o endereço onde o contrato SmartToken foi implantado
  console.log("SmartToken deployed to:", token.address);
  
  // Exibe no console o endereço do proxy do contrato SmartToken
  console.log("SmartToken Proxy deployed to:", token.address);
}

// Inicia a exportação do módulo
module.exports = {
  // Define a função 'main' como a exportação padrão do módulo
  default: main,
};

/*

Este arquivo é um script de implantação que utiliza o Hardhat. 
O objetivo deste script é compilar e implantar o contrato SmartToken na rede Ethereum.

*/