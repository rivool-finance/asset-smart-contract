// Este script é responsável por atualizar o contrato inteligente "SmartToken" para a versão mais recente "NewSmartToken".
// Primeiro, carregamos as bibliotecas necessárias do Hardhat.
const { ethers, upgrades } = require("hardhat");

// A seguir, definimos a função principal que será executada.
async function main() {
  // Aqui, definimos o endereço do contrato Proxy como uma constante.
  const proxyAddress = "PROXY_ADDRESS";

  // Exibimos uma mensagem na tela informando que o processo de atualização começou.
  console.log("Upgrading SmartToken...");

  // Criamos uma instância do contrato "NewSmartToken" usando a função "getContractFactory" da biblioteca "ethers".
  const NewSmartToken = await ethers.getContractFactory("NewSmartToken");

  // Atualizamos o contrato "SmartToken" para a versão mais recente "NewSmartToken" usando a função "upgradeProxy".
  const upgradedSmartToken = await upgrades.upgradeProxy(proxyAddress, NewSmartToken);

  // Exibimos o endereço do contrato atualizado na tela.
  console.log("SmartToken upgraded to NewSmartToken at:", upgradedSmartToken.address);
}

// Finalmente, executamos a função principal e verificamos se o processo de atualização foi concluído com sucesso ou não.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });

  /*

  Este é um script de atualização para um contrato inteligente (smart contract)
  escrito em JavaScript usando a biblioteca Hardhat. 
  O objetivo desse script é atualizar o contrato inteligente conhecido como "SmartToken"
  para uma versão mais recente, chamada "NewSmartToken".

  */
