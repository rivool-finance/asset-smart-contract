// Importa o objeto 'hre' do pacote 'hardhat'
const hre = require("hardhat");

// Define a função assíncrona 'main', que será executada para interagir com o contrato implantado
async function main() {
  // Obtenha os signatários (endereços) da rede local
  const [addr1, addr2] = await hre.ethers.getSigners();

  // Substitua 'contractAddress' pelo endereço do proxy implantado
  const contractAddress = "0xCf7Ed3AccA5a467e9e704C703E8D87F634fB0Fc9";

  // Conectar ao contrato já implantado através do proxy
  const TokenFactory = await hre.ethers.getContractFactory("SmartTokenV2");
  const token = TokenFactory.attach(contractAddress);
  console.log("Connected to SmartTokenV2 at:", contractAddress);

  // Configura a interface de linha de comando (CLI) para interagir com o usuário
  const rl = require("readline").createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  // Função para solicitar entrada do usuário
  async function prompt() {
    return new Promise((resolve) => {
      rl.question("Escolha a função para executar: (mint, approve, transferFrom, pay, withdraw, exit) ", (answer) => {
        resolve(answer);
      });
    });
  }

  // Variável para controlar a execução contínua do loop
  let continueExecution = true;

  // Loop para executar as funções do contrato com base na entrada do usuário
  while (continueExecution) {
    // Aguarda a resposta do usuário
    const answer = await prompt();

    // Executa a função do contrato com base na resposta do usuário
    if (answer === "mint") {
      // Chama a função 'mint' do contrato para emitir tokens para o endereço especificado
      await token.connect(addr1).mint('0x6CCc223006507d13e952caB700E4Ba89d675CBDd', hre.ethers.utils.parseUnits("100", 18));
      console.log("Minted 100 tokens to addr1");
    } else if (answer === "approve") {
      // Chama a função 'approve' do contrato para permitir que addr2 gaste tokens em nome de addr1
      await token.connect(addr1).approve(addr2.address, hre.ethers.utils.parseUnits("50", 18));
      console.log("Addr1 approved Addr2 to spend 50 tokens");
    } else if (answer === "transferFrom") {
      // Chama a função 'transferFrom' do contrato para transferir tokens de addr1 para addr2
      await token.connect(addr2).transferFrom(addr1.address, addr2.address, hre.ethers.utils.parseUnits("25", 18));
      console.log("Addr2 transferred 25 tokens from Addr1 to Addr2");
    } else if (answer === "pay") {
      // Chama a função 'pay' do contrato para realizar um pagamento com comprovante
      await token.connect(addr1).pay(addr2.address, hre.ethers.utils.parseUnits("10", 18), "payment_proof");
      console.log("Addr1 paid Addr2 10 tokens with payment proof");
    } else if (answer === "withdraw") {
      // Chama a função 'withdraw' do contrato para que addr2 retire tokens
      await token.connect(addr2).withdraw(hre.ethers.utils.parseUnits("5", 18));
      console.log("Addr2 withdrew 5 tokens");
    } else if (answer === "burnFrom") {
      // Chama a função 'burnFrom' do contrato para queimar tokens do endereço addr1
      await token.connect(addr1).burnFrom(addr1.address, hre.ethers.utils.parseUnits("10", 18));
      console.log("Burned 10 tokens from Addr1");
    } else if (answer === "exit") {
    // Encerra o loop e termina a execução do script
    console.log("Exiting...");
    continueExecution = false;
    } else {
    // Caso a entrada do usuário seja inválida, exibe uma mensagem de erro e solicita novamente
    console.log("Comando inválido. Tente novamente.");
    }
  }
  // Fecha a interface da linha de comando
  rl.close();
  }

  // Executa a função 'main' e encerra o processo em caso de sucesso ou erro
  main()
  .then(() => process.exit(0))
  .catch((error) => {
  console.error(error);
  process.exit(1);
  }
);
