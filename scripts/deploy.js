// Importa o objeto 'hre' do pacote 'hardhat'
const hre = require("hardhat");

// Define a função assíncrona 'main', que será executada para implantar os contratos
async function main() {
  // Implante o contrato SmartTokenV2
  // Obtém a fábrica de contratos do SmartTokenV2, que será usada para criar uma nova instância do contrato
  const TokenV2Factory = await hre.ethers.getContractFactory("SmartTokenV2");
  
  // Cria uma nova instância do contrato SmartTokenV2 e passa a URL da imagem do token como argumento
  const tokenV2 = await TokenV2Factory.deploy("https://example.com/token-image");
  
  // Aguarda até que o contrato SmartTokenV2 seja implantado com sucesso na rede Ethereum
  await tokenV2.deployed();
  
  // Exibe no console o endereço onde o contrato SmartTokenV2 foi implantado
  console.log("SmartTokenV2 deployed to:", tokenV2.address);

  // Implante o contrato TransparentUpgradeableProxy
  // Obtém a fábrica de contratos do TransparentUpgradeableProxy
  const ProxyFactory = await hre.ethers.getContractFactory(
    "@openzeppelin/contracts/proxy/transparent/TransparentUpgradeableProxy.sol:TransparentUpgradeableProxy"
  );

  // Substitua 'proxyAdminAddress' pelo endereço da conta que será o administrador do proxy.
  const proxyAdminAddress = "0xbfB8dB66D11cDc0C6d0912C44d714d448ecEe941";
  
  // Cria uma nova instância do contrato TransparentUpgradeableProxy e passa o endereço do tokenV2, o endereço do administrador e um array vazio como argumentos
  const proxy = await ProxyFactory.deploy(tokenV2.address, proxyAdminAddress, []);
  
  // Aguarda até que o contrato TransparentUpgradeableProxy seja implantado com sucesso na rede Ethereum
  await proxy.deployed();
  
  // Exibe no console o endereço onde o contrato TransparentUpgradeableProxy foi implantado
  console.log("TransparentUpgradeableProxy deployed to:", proxy.address);
}

// Inicia a execução da função 'main' e define o tratamento de erros e finalização do processo
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
