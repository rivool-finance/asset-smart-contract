// Integração com a biblioteca Waffle para facilitar os testes de contratos inteligentes
require("@nomiclabs/hardhat-waffle");

// Verificação de contrato inteligente no Etherscan e Polygonscan
require("@nomiclabs/hardhat-etherscan");

// Acesso ao objeto ethers para interagir facilmente com a blockchain Ethereum
require("@nomiclabs/hardhat-ethers");

// Importação do plugin hardhat-deploy
require("hardhat-deploy");

// Plugin hardhat para teste de proxy
require("@openzeppelin/hardhat-upgrades");

// Importar informações sensíveis do arquivo secrets.json
const {
  goerliRpcUrl,
  rpcUrl,
  privateKey,
  polygonscanApiKey,
  chainId,
  gas,
  gasPrice,
  etherscanApiKey,
} = require("./secrets.json");

module.exports = {
  // Configuração do compilador Solidity
  solidity: {
    compilers: [
      { version: "0.8.0" },
      { version: "0.8.18" },
    ],
  },

  // Seção de namedAccounts
  namedAccounts: {
    deployer: {
      default: 0,
    },
    proxy01Owner: {
      default: 1,
    },
    simpleERC20Beneficiary: {
      default: 2,
    },
  },

  // Configuração das redes suportadas pelo Hardhat
  networks: {
    polygon: {
      url: rpcUrl,
      chainId: chainId,
      gas: gas,
      gasPrice: gasPrice,
      accounts: [`0x${privateKey}`],
    },
    goerli: {
      url: goerliRpcUrl,
      chainId: 5,
      gas: gas,
      gasPrice: gasPrice,
      accounts: [`0x${privateKey}`],
    },
    mumbai: {
      url: `https://polygon-mumbai.g.alchemy.com/v2/o_NEWGs-_UBO6ghggx8ykcX4SjX7oBd2`, // Alchemy URL for Mumbai
      accounts: [`0x${privateKey}`],
    },
  },

  // Configuração do Etherscan e Polygonscan
  etherscan: {
    apiKey: etherscanApiKey,
    polygonscan: {
      apiKey: polygonscanApiKey,
      url: "https://api-testnet.polygonscan.com/",
    },
  },
};
