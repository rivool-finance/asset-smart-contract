// Importa as funções 'expect' e 'ethers' do módulo 'chai' e 'hardhat' respectivamente
const { expect } = require("chai");
const { ethers } = require("hardhat");

// Inicia o bloco de testes com o nome "SmartToken"
describe("SmartToken", function () {
  // Declara as variáveis que serão usadas durante os testes
  let SmartToken, smartToken, owner, addr1, addr2;

  // Antes de cada teste, executa as seguintes ações:
  beforeEach(async function () {
    // Obtém a fábrica de contratos do tipo "SmartToken"
    SmartToken = await ethers.getContractFactory("SmartToken");
    // Define as informações básicas do token (nome, símbolo, URL da imagem)
    const name = "Smart Token";
    const symbol = "BRLTS";
    const tokenImage = "https://imgur.com/a/fuzZCh8";
    // Despacha o contrato SmartToken com as informações definidas
    smartToken = await SmartToken.deploy(name, symbol, tokenImage);
    // Aguarda o contrato ser despachado completamente
    await smartToken.deployed();
  
    // Obtém o endereço de 3 contas (proprietário, addr1 e addr2) para uso nos testes
    [owner, addr1, addr2] = await ethers.getSigners();
  });
  
  // Teste de criação de tokens (função mint)
  describe("Mint function", function () {
    it("Should mint tokens to a valid address", async function () {
      const initialSupply = await smartToken.totalSupply();
      const mintAmount = ethers.utils.parseEther("1000");
      await smartToken.mint(addr1.address, mintAmount);
      const addr1Balance = await smartToken.balanceOf(addr1.address);
      expect(addr1Balance).to.equal(mintAmount);
      const finalSupply = await smartToken.totalSupply();
      expect(finalSupply).to.equal(initialSupply.add(mintAmount));
    });

    it("Should fail when minting tokens above the max supply", async function () {
      const maxSupply = ethers.utils.parseEther("10000000");
      const mintAmount = maxSupply.add(ethers.utils.parseEther("1"));
      await expect(smartToken.mint(addr1.address, mintAmount)).to.be.revertedWith("Total supply limit exceeded");
    });

    it("Should fail when a non-owner tries to mint tokens", async function () {
      const smartTokenFromAddr1 = smartToken.connect(addr1);
      const mintAmount = ethers.utils.parseEther("1000");
      await expect(smartTokenFromAddr1.mint(addr1.address, mintAmount)).to.be.revertedWith("Ownable: caller is not the owner");
    });
  });

  // Teste de transferência de tokens (função transfer)
  describe("Transfer function", function () {
    beforeEach(async function () {
      const mintAmount = ethers.utils.parseEther("1000");
      await smartToken.mint(owner.address, mintAmount);
    });

    it("Should transfer tokens from the owner to another address", async function () {
      const transferAmount = ethers.utils.parseEther("100");
      await smartToken.transfer(addr1.address, transferAmount);
      const addr1Balance = await smartToken.balanceOf(addr1.address);
      expect(addr1Balance).to.equal(transferAmount);
    });

    it("Should fail when trying to transfer more tokens than the sender's balance", async function () {
      const transferAmount = ethers.utils.parseEther("2000");
      await expect(smartToken.transfer(addr1.address, transferAmount)).to.be.revertedWith("ERC20: transfer amount exceeds balance");
    });
  });

  // Teste de aprovação de gastos (função approve)
  describe("Approve function", function () {
    beforeEach(async function () {
      const mintAmount = ethers.utils.parseEther("1000");
      await smartToken.mint(owner.address, mintAmount);
    });

    it("Should approve a spender to spend a certain amount of tokens on behalf of the owner", async function () {
      const approveAmount = ethers.utils.parseEther("100");
      await smartToken.approve(addr1.address, approveAmount);
      const allowance = await smartToken.allowance(owner.address, addr1.address);
      expect(allowance).to.equal(approveAmount);
    });
    });
    
    // Teste de consulta de permissão de gastos (função allowance)
    describe("Allowance function", function () {
    beforeEach(async function () {
    const mintAmount = ethers.utils.parseEther("1000");
    await smartToken.mint(owner.address, mintAmount);
    const approveAmount = ethers.utils.parseEther("100");
    await smartToken.approve(addr1.address, approveAmount);
    });

    it("Should return the correct allowance for a spender", async function () {
        const allowance = await smartToken.allowance(owner.address, addr1.address);
        expect(allowance).to.equal(ethers.utils.parseEther("100"));
      });
    });

    // Teste de transferência de tokens em nome de outro endereço (função transferFrom)
    describe("TransferFrom function", function () {
    beforeEach(async function () {
    const mintAmount = ethers.utils.parseEther("1000");
    await smartToken.mint(owner.address, mintAmount);
    const approveAmount = ethers.utils.parseEther("100");
    await smartToken.approve(addr1.address, approveAmount);
    });

    it("Should transfer tokens on behalf of the owner to another address", async function () {
        const smartTokenFromAddr1 = smartToken.connect(addr1);
        const transferAmount = ethers.utils.parseEther("50");
        await smartTokenFromAddr1.transferFrom(owner.address, addr2.address, transferAmount);
        const addr2Balance = await smartToken.balanceOf(addr2.address);
        expect(addr2Balance).to.equal(transferAmount);
      });
      
      it("Should fail when trying to transfer more tokens than allowed", async function () {
        const smartTokenFromAddr1 = smartToken.connect(addr1);
        const transferAmount = ethers.utils.parseEther("200");
        await expect(smartTokenFromAddr1.transferFrom(owner.address, addr2.address, transferAmount)).to.be.revertedWith("ERC20: transfer amount exceeds allowance");
      });
    });

    // Teste de pagamento com comprovante (função pay)
    describe("Pay function", function () {
    beforeEach(async function () {
    const mintAmount = ethers.utils.parseEther("1000");
    await smartToken.mint(owner.address, mintAmount);
    });

    it("Should make a payment and emit a Payment event", async function () {
      const transferAmount = ethers.utils.parseEther("100");
      const paymentProof = "ExamplePaymentProof";
      const latestBlock = await ethers.provider.getBlock('latest');
      
      const tx = await smartToken.pay(addr1.address, transferAmount, paymentProof);
      const receipt = await tx.wait();
      const event = receipt.events.find((e) => e.event === "Payment");
      
      // Adicione esta linha para converter a string hexadecimal de volta para uma string normal
      const decodedPaymentProof = ethers.utils.toUtf8String(event.args.paymentProof);
      
      // Função personalizada para remover caracteres nulos do final da string
      const removeTrailingNulls = (str) => str.replace(/\u0000+$/, '');
      
      // Remova os caracteres nulos no final da string
      const trimmedDecodedPaymentProof = removeTrailingNulls(decodedPaymentProof);
      
      expect(event.args.sender).to.equal(owner.address);
      expect(event.args.recipient).to.equal(addr1.address);
      expect(event.args.amount).to.equal(transferAmount);
      expect(trimmedDecodedPaymentProof).to.equal(paymentProof); // Altere esta linha para usar 'trimmedDecodedPaymentProof'
      expect(event.args.timestamp).to.be.closeTo(latestBlock.timestamp, 2);
    });
          
    it("Should fail when trying to make a payment with insufficient balance", async function () {
        const transferAmount = ethers.utils.parseEther("2000");
        const paymentProof = "ExamplePaymentProof";
        await expect(smartToken.pay(addr1.address, transferAmount, paymentProof)).to.be.revertedWith("Insufficient balance");
      });
    });

    // Teste de saque de tokens (função withdraw)
    describe("Withdraw function", function () {
    beforeEach(async function () {
    const mintAmount = ethers.utils.parseEther("1000");
    await smartToken.mint(owner.address, mintAmount);
    });

    it("Should withdraw tokens and reduce the balance", async function () {
        const initialBalance = await smartToken.balanceOf(owner.address);
        const withdrawAmount = ethers.utils.parseEther("100");
        await smartToken.withdraw(withdrawAmount);
        const finalBalance = await smartToken.balanceOf(owner.address);
        expect(finalBalance).to.equal(initialBalance.sub(withdrawAmount));
    });
    
    it("Should fail when trying to withdraw more tokens than the balance", async function () {
        const withdrawAmount = ethers.utils.parseEther("2000");
        await expect(smartToken.withdraw(withdrawAmount)).to.be.revertedWith("Insufficient balance");
    });
    
    it("Should fail when a non-owner tries to withdraw tokens", async function () {
        const smartTokenFromAddr1 = smartToken.connect(addr1);
        const withdrawAmount = ethers.utils.parseEther("100");
        await expect(smartTokenFromAddr1.withdraw(withdrawAmount)).to.be.revertedWith("Insufficient balance");
    });
});

// Teste de consulta de URL da imagem do token (função tokenImage)
describe("TokenImage function", function () {
it("Should return the correct token image URL", async function () {
const imageUrl = await smartToken.tokenImage();
expect(imageUrl).to.equal("https://imgur.com/a/fuzZCh8");
});
});
});