// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;

// Importando contratos do OpenZeppelin
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20BurnableUpgradeable.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

// Contrato SmartToken que herda as funcionalidades dos contratos ERC20, ERC20Burnable e Ownable
contract SmartToken is ERC20Upgradeable, ERC20BurnableUpgradeable, OwnableUpgradeable {
    // Mapeamento para armazenar a quantidade de tokens que um endereço tem autorização para gastar em nome de outro endereço
    mapping (address => mapping (address => uint256)) private _authorized;

    // Definindo a quantidade de casas decimais do token
    uint8 private constant _decimals = 18;
    // Definindo o limite máximo para a quantidade total de tokens que podem ser emitidos
    uint256 private constant _maxSupply = 10000000 * 10 ** uint256(_decimals);
    // Variável para armazenar a imagem do token
    string public tokenImage;

    // Evento Payment emitido quando um pagamento é realizado com comprovante
    event Payment(address indexed sender, address indexed recipient, uint256 amount, bytes32 paymentProof, uint256 timestamp);

    // Função initialize para inicializar o contrato com nome, símbolo e imagem do token
    function initialize(string memory name, string memory symbol, string memory _tokenImage) public initializer {
        __ERC20_init_unchained(name, symbol);
        __Ownable_init();
        _mint(address(this), 50000 * 10 ** _decimals);
        tokenImage = _tokenImage;
    }

    // Função para criar (mint) tokens, restrita ao proprietário do contrato
    function mint(address to, uint256 amount) public onlyOwner {
        require(totalSupply() + amount <= _maxSupply, "Total supply limit exceeded");
        _mint(to, amount);
    }

    // Função de transferência de tokens restrita ao proprietário do contrato
    function transfer(address recipient, uint256 amount) public virtual override onlyOwner returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    // Função para dar permissão a outro endereço gastar tokens em nome do proprietário, restrita ao proprietário do contrato
    function approve(address spender, uint256 amount) public virtual override onlyOwner returns (bool) {
        _authorized[_msgSender()][spender] = amount;
        emit Approval(_msgSender(), spender, amount);
        return true;
    }

    // Função para verificar a quantidade de tokens que um endereço tem permissão para gastar em nome de outro endereço
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _authorized[owner][spender];
    }

    // Função para transferir tokens de um endereço para outro (transferFrom), restrita ao proprietário do contrato
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override onlyOwner returns (bool) {
        require(_authorized[sender][_msgSender()] >= amount, "ERC20: transfer amount exceeds allowance");
        _transfer(sender, recipient, amount);
        _authorized[sender][_msgSender()] -= amount;
        emit Approval(sender, _msgSender(), _authorized[sender][_msgSender()]);
        return true;
    }

    // Função para realizar pagamentos com comprovante
    function pay(address recipient, uint256 amount, string memory paymentProof) public returns (bool) {
        require(balanceOf(msg.sender) >= amount, "Insufficient balance");
        require(transfer(recipient, amount), "Transfer failed");
        uint256 timestamp = block.timestamp;
        emit Payment(msg.sender, recipient, amount, stringToBytes32(paymentProof), timestamp);
        return true;
    }

    // Função para converter strings em bytes32
    function stringToBytes32(string memory source) private pure returns (bytes32 result) {
        bytes memory tempEmptyStringTest = bytes(source);
        if (tempEmptyStringTest.length == 0) {
            return 0x0;
        }
        assembly {
            result := mload(add(source, 32))
        }
    }

    // Função para sacar (burn) tokens, reduzindo o saldo do usuário
    function withdraw(uint256 amount) public returns (bool) {
        require(balanceOf(msg.sender) >= amount, "Insufficient balance");
        _burn(msg.sender, amount);
        return true;
    }

    // Função para atualizar a imagem do token, restrita ao proprietário do contrato
    function setTokenImage(string memory _tokenImage) public onlyOwner {
        tokenImage = _tokenImage;
    }
}
