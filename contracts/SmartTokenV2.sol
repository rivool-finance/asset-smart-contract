// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

// Define o contrato SmartTokenV2 que herda de ERC20 e Ownable
contract SmartTokenV2 is ERC20, Ownable {
    mapping (address => mapping (address => uint256)) private _authorized;
    
    uint8 private constant _decimals = 18;
    // Define o limite máximo para a quantidade total de tokens que podem ser emitidos
    uint256 private constant _maxSupply = 100000000000 * 10 ** uint256(_decimals);
    string public tokenImage;

    event Payment(address indexed sender, address indexed recipient, uint256 amount, string paymentProof, uint256 timestamp);

    // Construtor do SmartTokenV2, define o nome e símbolo do token e a imagem do token
    constructor(string memory _tokenImage) ERC20("SmartToken", "BRLTS") {
        // Define o balanço inicial do contrato
        _mint(address(this), 50000 * 10 ** _decimals);
        tokenImage = _tokenImage;
    }

    // Função para criar novos tokens
    function mint(address to, uint256 amount) public onlyOwner {
        require(totalSupply() + amount <= _maxSupply, "Total supply limit exceeded");
        _mint(to, amount);
    }

    // Função de transferência restrita apenas ao proprietário do contrato
    function transfer(address recipient, uint256 amount) public virtual override returns (bool) {
        _transfer(_msgSender(), recipient, amount);
        return true;
    }

    // Função para aprovar outro endereço para gastar tokens em nome do proprietário
    function approve(address spender, uint256 amount) public virtual override returns (bool) {
        _authorized[_msgSender()][spender] = amount;
        emit Approval(_msgSender(), spender, amount);
        return true;
    }

    // Função para verificar a quantidade de tokens que um endereço tem permissão para gastar em nome do proprietário
    function allowance(address owner, address spender) public view virtual override returns (uint256) {
        return _authorized[owner][spender];
    }

    // Função transferFrom restrita ao proprietário do contrato
    function transferFrom(address sender, address recipient, uint256 amount) public virtual override returns (bool) {
        require(_authorized[sender][_msgSender()] >= amount, "ERC20: transfer amount exceeds allowance");
        _transfer(sender, recipient, amount);
        _authorized[sender][_msgSender()] -= amount;
        emit Approval(sender, _msgSender(), _authorized[sender][_msgSender()]);
        return true;
    }

    // Função para efetuar um pagamento com comprovante
    function pay(address recipient, uint256 amount, string memory paymentProof) public returns (bool) {
        require(balanceOf(msg.sender) >= amount, "Insufficient balance");
        super.transfer(recipient, amount); // Substitua 'transfer' por 'super.transfer'
        uint256 timestamp = block.timestamp;
        emit Payment(msg.sender, recipient, amount, paymentProof, timestamp);
        return true;
    }

    // Função para sacar tokens
    function withdraw(uint256 amount) public returns (bool) {
        require(balanceOf(msg.sender) >= amount, "Insufficient balance");
        _burn(msg.sender, amount);
        return true; // Adicione o ponto e vírgula aqui
    }


    // Função para atualizar a variável tokenImage
    function setTokenImage(string memory _tokenImage) public onlyOwner {
        tokenImage = _tokenImage;
    }

    // Função burnFrom para queimar tokens de uma determinada conta (nova função para testar o proxy)
    function burnFrom(address account, uint256 amount) public onlyOwner {
        require(balanceOf(account) >= amount, "Insufficient balance");
        _burn(account, amount);
    }
}

/* 

O contrato `SmartTokenV2.sol` é uma implementação do token ERC20 com funcionalidades adicionais
como a possibilidade de criar novos tokens (mint), aprovar outros endereços para gastar tokens
em nome do proprietário (approve), transferir tokens de um endereço para outro (transferFrom)
efetuar pagamentos com comprovantes (pay) e sacar tokens (withdraw). Além disso, o contrato permite
que o proprietário atualize a imagem do token (tokenImage). O contrato foi projetado para ser atualizado através de um proxy
permitindo que novas funcionalidades sejam adicionadas no futuro sem afetar o estado atual do contrato.

*/
