// SPDX-License-Identifier: MIT
pragma solidity ^0.8.18;

// Importa o contrato ProxyAdmin da biblioteca OpenZeppelin
import "@openzeppelin/contracts/proxy/transparent/ProxyAdmin.sol";

// Define o contrato SmartTokenProxyAdmin, que herda do contrato ProxyAdmin
contract SmartTokenProxyAdmin is ProxyAdmin {
    // O construtor do SmartTokenProxyAdmin
    constructor() ProxyAdmin() {
        // Podemos adicionar código a mais aqui, se necessário
    }
}

/* 

O contrato SmartTokenProxyAdmin.sol é uma extensão do contrato ProxyAdmin fornecido pela biblioteca OpenZeppelin. 
O contrato ProxyAdmin gerencia a lógica de atualização dos contratos proxy transparentes
permitindo que você atualize a implementação do contrato sem perder o estado ou os dados armazenados.

*/